class CreateBadges < ActiveRecord::Migration
  def change
    create_table :badges do |t|
      t.string :image_link
      t.string :name
      t.integer :value

      t.timestamps
    end
  end
end
