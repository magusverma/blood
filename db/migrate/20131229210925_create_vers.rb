class CreateVers < ActiveRecord::Migration
  def change
    create_table :vers do |t|
      t.string :name

      t.timestamps
    end
  end
end
