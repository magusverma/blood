class CreateSeeks < ActiveRecord::Migration
  def change
    create_table :seeks do |t|
      t.integer :user_id
      t.string :patient_name
      t.integer :contact_number
      t.string :address
      t.string :medical_issue
      t.float :latitude
      t.float :longitude
      t.string :city
      t.string :state
      t.string :country
      t.string :hospital
      t.string :hospital_address
      t.integer :pincode

      t.timestamps
    end
  end
end
