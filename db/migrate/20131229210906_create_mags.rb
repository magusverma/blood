class CreateMags < ActiveRecord::Migration
  def change
    create_table :mags do |t|
      t.string :name

      t.timestamps
    end
  end
end
