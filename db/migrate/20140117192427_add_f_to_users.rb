class AddFToUsers < ActiveRecord::Migration
  def change
    add_column :users, :bg, :string
    add_column :users, :phone, :integer
    add_column :users, :address, :string
    add_column :users, :lat, :float
    add_column :users, :long, :float
  end
end
