class RemoveMagIdFromVer < ActiveRecord::Migration
  def change
    remove_column :vers, :mag_id, :integer
  end
end
