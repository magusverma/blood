class CreateFoobars < ActiveRecord::Migration
  def change
    create_table :foobars do |t|
      t.string :foo
      t.string :bar

      t.timestamps
    end
  end
end
