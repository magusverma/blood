class AddSomeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :latitude, :float
    add_column :users, :longitude, :float
    add_column :users, :points, :integer
    add_column :users, :redeemable, :integer
  end
end
