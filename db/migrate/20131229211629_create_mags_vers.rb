class CreateMagsVers < ActiveRecord::Migration
  def change
    create_table :mags_vers, :id => false do |t|
      t.integer :mag_id
      t.integer :ver_id

      t.timestamps
    end
  end
end
