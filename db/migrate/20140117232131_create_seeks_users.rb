class CreateSeeksUsers < ActiveRecord::Migration
  def change
    create_table :seeks_users do |t|
      t.integer :seek_id
      t.integer :user_id

      t.timestamps
    end
  end
end
