# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140117232131) do

  create_table "badges", force: true do |t|
    t.string   "image_link"
    t.string   "name"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "badges_users", force: true do |t|
    t.integer  "badge_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bids", force: true do |t|
    t.integer  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "foobars", force: true do |t|
    t.string   "foo"
    t.string   "bar"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mags", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ver_id"
  end

  create_table "mags_vers", id: false, force: true do |t|
    t.integer  "mag_id"
    t.integer  "ver_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "microposts", force: true do |t|
    t.string   "content"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "players", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "scaffs", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seeks", force: true do |t|
    t.integer  "user_id"
    t.string   "patient_name"
    t.integer  "contact_number"
    t.string   "address"
    t.string   "medical_issue"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "hospital"
    t.string   "hospital_address"
    t.integer  "pincode"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seeks_users", force: true do |t|
    t.integer  "seek_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "teams", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.integer  "uid"
    t.string   "bg"
    t.integer  "phone"
    t.string   "address"
    t.float    "lat"
    t.float    "long"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "points"
    t.integer  "redeemable"
  end

  create_table "vers", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
