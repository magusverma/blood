require 'test_helper'

class SeeksControllerTest < ActionController::TestCase
  setup do
    @seek = seeks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:seeks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create seek" do
    assert_difference('Seek.count') do
      post :create, seek: { address: @seek.address, city: @seek.city, contact_number: @seek.contact_number, country: @seek.country, hospital: @seek.hospital, hospital_address: @seek.hospital_address, latitude: @seek.latitude, longitude: @seek.longitude, medical_issue: @seek.medical_issue, patient_name: @seek.patient_name, pincode: @seek.pincode, state: @seek.state, user_id: @seek.user_id }
    end

    assert_redirected_to seek_path(assigns(:seek))
  end

  test "should show seek" do
    get :show, id: @seek
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @seek
    assert_response :success
  end

  test "should update seek" do
    patch :update, id: @seek, seek: { address: @seek.address, city: @seek.city, contact_number: @seek.contact_number, country: @seek.country, hospital: @seek.hospital, hospital_address: @seek.hospital_address, latitude: @seek.latitude, longitude: @seek.longitude, medical_issue: @seek.medical_issue, patient_name: @seek.patient_name, pincode: @seek.pincode, state: @seek.state, user_id: @seek.user_id }
    assert_redirected_to seek_path(assigns(:seek))
  end

  test "should destroy seek" do
    assert_difference('Seek.count', -1) do
      delete :destroy, id: @seek
    end

    assert_redirected_to seeks_path
  end
end
