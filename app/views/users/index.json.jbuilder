json.array!(@users) do |user|
  json.extract! user, :email, :bg, :phone, :address
  json.url user_url(user, format: :json)
end