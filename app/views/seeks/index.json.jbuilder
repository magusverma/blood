json.array!(@seeks) do |seek|
  json.extract! seek, :id, :user_id, :patient_name, :contact_number, :address, :medical_issue, :latitude, :longitude, :city, :state, :country, :hospital, :hospital_address, :pincode
  json.url seek_url(seek, format: :json)
end
