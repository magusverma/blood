json.array!(@badges) do |badge|
  json.extract! badge, :id, :image_link, :name, :value
  json.url badge_url(badge, format: :json)
end
