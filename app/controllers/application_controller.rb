class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  def index
  	@current_user ||= User.find(session[:user_id]) if session[:user_id]
    redirect_to home_path, notice: 'Succesfully logged in' unless @current_user.nil?
  end

  def home
    
  end

  def edit
    edit_user_path(current_user)
  end

  protect_from_forgery with: :exception
	private

	def current_user
	  @current_user ||= User.find(session[:user_id]) if session[:user_id]
	end
helper_method :current_user
end
