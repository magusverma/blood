class SeeksController < ApplicationController
  # GET /seeks
  # GET /seeks.json
  def index
    @seeks = Seek.all
  end

  def donate
    s = Seek.find_by_id(params[:id])
    s.users.append(current_user)
    current_user.points += 100
    current_user.redeemable += 100
    current_user.save
    #redirect_to "/",notice: 'You just got more cash'
  end
  # GET /seeks/1
  # GET /seeks/1.json
  def show
    @seek = Seek.find(params[:id])
    @supporters = @seek.users
  end

  # GET /seeks/new
  def new
    @seek = Seek.new
  end

  # GET /seeks/1/edit
  def edit
  end

  # POST /seeks
  # POST /seeks.json
  def create
    #uset = current user's id
    @seek = Seek.new(seek_params)
    current_user.points += 20
    current_user.redeemable += 20
    current_user.save
    respond_to do |format|
      if @seek.save
        format.html { redirect_to @seek, notice: 'Seek was successfully created.' }
        format.json { render action: 'show', status: :created, location: @seek }
      else
        format.html { render action: 'new' }
        format.json { render json: @seek.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /seeks/1
  # PATCH/PUT /seeks/1.json
  def update
    respond_to do |format|
      if @seek.update(seek_params)
        format.html { redirect_to @seek, notice: 'Seek was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @seek.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /seeks/1
  # DELETE /seeks/1.json
  def destroy
    @seek.destroy
    respond_to do |format|
      format.html { redirect_to seeks_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_seek
      @seek = Seek.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def seek_params
      params.require(:seek).permit(:user_id, :patient_name, :contact_number, :address, :medical_issue, :latitude, :longitude, :city, :state, :country, :hospital, :hospital_address, :pincode)
    end
  
end