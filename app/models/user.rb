class User < ActiveRecord::Base
	has_and_belongs_to_many :seeks
	has_and_belongs_to_many :badges
	#validates :email, presence: true
	geocoded_by :address
	  after_validation :geocode, :if => :address_changed?
	def self.from_omniauth(auth)
	  where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
	    user.provider = auth.provider
	    user.uid = auth.uid
	    user.name = auth.info.name
	    user.points = 10 if user.points.nil?
	    user.redeemable = 10 if user.redeemable.nil?
	    #user.email = auth.extra.user_hash.email
	    user.oauth_token = auth.credentials.token
	    user.oauth_expires_at = Time.at(auth.credentials.expires_at)
	    user.save!
	  end
	end
end
